# 0.2.0 2017-8-8

* Signal can now be specified via flag

# 0.1.2 2017-8-8

* Extra spaces are now printed on startup to make space for all the lines
* Cursor position is now saved before printing so that the cursor returns correctly

# 0.1.1 2017-8-8

* Removed `errln` dependency in favour of the standard eprintln
* Updated regex, strsim, and nix dependencies
* Printing now takes terminal width into account

# 0.1.0 2016-12-15

* Initial release