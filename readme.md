# Hitman

[![build status](https://gitlab.com/timlyo/Hitman/badges/master/build.svg)](https://gitlab.com/timlyo/Hitman/commits/master)


Let us kill that process for you

![Hitman](https://i.imgur.com/3BAzklh.png)

Hitman is an interactive process killer for Linux, designed to make it quick and easy to kill a single process. Just start typing the name or commandline of your process, and press enter to kill.

# To Use

Start typing the name or commandline of the process that you want to kill. Up and down arrows to select, and enter to unleash.

Kill parameters can be specified by appending a number to the command e.g. `hitman 9` sends a SIGKILL. 

# Install

## Binary

A precompiled binary for `x86_64` is available from [here](https://gitlab.com/timlyo/Hitman/builds/artifacts/master/raw/target/release/hitman?job=build)

## Packages

Available from the AUR

https://aur.archlinux.org/packages/hitman/

## Manual Build

```
git clone https://gitlab.com/timlyo/Hitman.git
cd hitman
cargo build --release
```

The binary will be produced in `target/release` and can then be moved into a binary directory such as `/usr/bin`

