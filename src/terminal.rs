use std::io;
use std::io::{Read, Write};
use terminal_size::terminal_size;

use termios::{tcsetattr, Termios, ECHO, ICANON, TCSANOW};

const STDIN: i32 = 0;

/// Get a single character from stdin
pub fn get_char() -> char {
    let stdout = io::stdout();
    let mut reader = io::stdin();

    let mut buffer = [0; 1];
    stdout.lock().flush().unwrap();
    reader.read_exact(&mut buffer).unwrap();

    return buffer[0] as char;
}

pub fn setup() -> Result<Termios, io::Error> {
    let mut termios = Termios::from_fd(STDIN)?;
    termios.c_lflag &= !(ICANON | ECHO);
    tcsetattr(STDIN, TCSANOW, &mut termios)?;

    Ok(termios)
}

pub fn reset(termios: &Termios) {
    tcsetattr(STDIN, TCSANOW, &termios).unwrap();
}

/// Return width of the current terminal
pub fn get_width() -> u16 {
    let size = terminal_size();
    match size {
        Some((width, _)) => width.0,
        None => 80,
    }
}

pub fn save_position() {
    print!("\x1b[s");
    flush();
}

pub fn restore_position() {
    print!("\x1b[u");
    flush();
}

pub fn clear_below_cursor() {
    print!("\x1b[J");
    flush();
}

pub fn move_right(amount: u32) {
    print!("\x1b[{}C", amount);
    flush();
}

pub fn clear_line() {
    print!("\x1b[2K");
    flush();
}

pub fn bold_and_underline() {
    print!("\x1b[1;4m");
}

/// Prints empty lines to make space
pub fn make_space() {
    for _ in 0..16 {
        println!();
    }
    print!("\x1b[16A");
    flush();
}

pub fn flush() {
    let mut stdout = io::stdout();
    let _ = stdout.flush();
}
