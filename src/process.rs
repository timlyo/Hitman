use std::fmt;
use std::fs;
use std::io;
use std::io::Read;
use strsim::jaro;

#[derive(Debug)]
pub struct Process {
    /// Process pid
    pub pid: u64,
    /// Parameters passed to the process
    pub cmdline: String,
    /// Name of the executable
    pub executable: String,
}

impl Process {
    pub fn from_pid(pid: u64) -> Result<Process, io::Error> {
        let cmdline = get_cmdline(pid)?;

        let file_name = format!("/proc/{}/exe", pid);
        let executable = fs::read_link(&file_name)?
            // file_name only returns None if the path terminates in ..
            .file_name()
            .expect(&format!("{} had no name", file_name))
            .to_str()
            .ok_or(io::Error::new(
                io::ErrorKind::InvalidData,
                "Executable is not valid utf-8",
            ))?.to_string();

        Ok(Process {
            pid,
            cmdline,
            executable,
        })
    }

    /// Simple score function used for ordering output
    pub fn score_process(&self, filter: &str) -> i64 {
        let mut score = 0;

        // If exactly equal
        if self.cmdline == filter {
            score += 100;
        }

        // Add points for similarity to executable name
        score += (jaro(&self.executable, filter) * 100.0) as i64;

        // Add points for if commandline contains the search string
        if self.cmdline.contains(filter) {
            score += 80;
        }

        if score > 1000 {
            0
        } else {
            10000 - score // TODO custom ord function
        }
    }
}

/// Load the commandline from /proc/ for a specific pid
pub fn get_cmdline(pid: u64) -> Result<String, io::Error> {
    let cmdline_file = format!("/proc/{}/cmdline", pid);
    let mut cmdline = String::new();

    fs::File::open(cmdline_file)?.read_to_string(&mut cmdline)?;

    Ok(cmdline)
}

impl fmt::Display for Process {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let width = self.executable.len() + 5;
        let precision = f.precision().unwrap_or(70) - width;
        let cmdline = {
            if self.cmdline.len() > precision {
                &self.cmdline[..precision]
            } else {
                &self.cmdline
            }
        };

        write!(f, "-{} -- {}", self.executable, cmdline)
    }
}
