use nix::sys::signal::{kill, Signal};
use nix::unistd::Pid;
use process::Process;
use std::fs;
use std::io;

/// Return a list of valid processes from /proc
pub fn get_process_list() -> Result<Vec<Process>, io::Error> {
    Ok(fs::read_dir("/proc/")?
        // Filter out errored entries
        .filter_map(|dir_entry| dir_entry.ok())
        // Convert names from OsString to String
        .filter_map(|dir_entry| dir_entry.file_name().into_string().ok())
        // Convert to a number
        .filter_map(|pid| pid.parse().ok())
        // Convert to process
        .filter_map(|pid| Process::from_pid(pid).ok())
        .collect())
}

#[must_use]
pub fn kill_process(process: &Process, signal: Signal) -> Result<(), nix::Error> {
    let pid = Pid::from_raw(process.pid as i32);
    kill(pid, signal)
}
