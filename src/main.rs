extern crate clap;
extern crate nix;
extern crate strsim;
extern crate terminal_size;
extern crate termios;

mod process;
mod system;
mod terminal;

use nix::sys::signal::Signal;
use process::Process;
use std::io;
use std::convert::TryFrom;

const LIST_LENGTH: usize = 15;

fn main() -> Result<(), io::Error> {
    let termios = terminal::setup()?;

    // Array for user input
    let mut input = String::new();

    // Index of selected item
    let mut selected: usize = 0;
    let mut process_list = system::get_process_list()?;

    let parameters = parse_commandline();

    terminal::make_space();
    terminal::save_position();

    loop {
        process_list.sort_by_key(|x| x.score_process(&input));

        let action = map_char_to_action(terminal::get_char());
        // result of input character
        // Ok(true) if esc was pressed
        let result = handle_character(
            action,
            &process_list,
            &parameters,
            &mut input,
            &mut selected,
        );

        match result {
            Ok(true) => break,
            Ok(false) => {}
            Err(e) => eprintln!("Error killing process: {:?}", e),
        }

        terminal::restore_position();
        terminal::clear_line();
        println!("{}", input);
        print_process_list(&process_list, selected);
        terminal::restore_position();
        // Move cursor to end of input
        terminal::move_right(input.len() as u32);
    }

    terminal::restore_position();
    println!("Your wish is my command");
    terminal::clear_below_cursor();
    terminal::reset(&termios);

    Ok(())
}

#[derive(Debug)]
enum Action {
    Backspace,
    Run,
    KeyPress,
    AddChar(char),
}

fn map_char_to_action(input: char) -> Action {
    match input {
        '\u{7f}' => Action::Backspace,
        '\n' => Action::Run,
        '\u{1b}' => Action::KeyPress,
        _ => Action::AddChar(input),
    }
}

/// Handle input, return true if the program should exit
fn handle_character(
    action: Action,
    process_list: &Vec<Process>,
    parameters: &Parameters,
    input: &mut String,
    selected: &mut usize,
) -> Result<bool, nix::Error> {
    match action {
        Action::Backspace => {input.pop();},
        Action::Run => {
            return system::kill_process(&process_list[*selected], parameters.signal).map(|_| true)
        }
        Action::KeyPress => handle_escape(selected),
        Action::AddChar(character) => input.push(character),
    }
    Ok(false)
}

fn handle_escape(selected: &mut usize) {
    if terminal::get_char() != '[' {
        return;
    }

    match terminal::get_char() {
        'A' => match *selected {
            0 => *selected = LIST_LENGTH - 1,
            _ => *selected -= 1,
        },
        'B' => match *selected {
            14 => *selected = 0,
            _ => *selected += 1,
        },
        //'C' => println!("Right"),
        //'D' => println!("Left"),
        _ => {}
    }
}

fn print_process_list(processes: &Vec<Process>, selected: usize) {
    let width = terminal::get_width() as usize;
    for (index, process) in processes.iter().take(LIST_LENGTH).enumerate() {
        if index == selected {
            terminal::bold_and_underline();
        }
        println!("\x1b[K{:.width$}", process, width = width);
        if index == selected {
            print!("\x1b[0m"); // Reset formatting
        }
    }
}

#[derive(Debug)]
struct Parameters {
    signal: Signal,
}

fn parse_commandline() -> Parameters {
    let matches = clap::App::new("Hitman")
        .arg(
            clap::Arg::with_name("signal")
                .short("s")
                .long("signal")
                .takes_value(true)
                .default_value("15"),
        ).get_matches();

    let signal: i32 = matches.value_of("signal").unwrap().parse().unwrap_or(15);

    Parameters {
        signal: Signal::try_from(signal).expect("Invalid signal value")
    }
}
